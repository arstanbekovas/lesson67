import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://lesson68-cf507.firebaseio.com/'


});

export default  instance;